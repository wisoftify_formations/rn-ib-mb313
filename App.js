import React from 'react';
import {NavigationContainer} from "@react-navigation/native";
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs"
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { Text, Image } from 'react-native';
import ActuIcon from "./assets/newspaper.png";
import { Provider } from './context/ui';

import Actus from "./page/Actus";//index.js is implicit
import Counter from './page/Counter';

const Tab = createBottomTabNavigator();

const Equipes = () => <Text>Equipes</Text>

const App = () => {
  return (
    <Provider>
      <NavigationContainer>
        <Tab.Navigator initialRouteName='actus'>
          <Tab.Screen name="actus" component={Actus} options={{
            tabBarIcon: ({color, size}) => (<Image source={ActuIcon} style={{width: 30, height: 30}} />)
          }} />
          <Tab.Screen name="equipes" component={Equipes} options={{
            tabBarIcon: ({color, size}) => (<MaterialCommunityIcons name="soccer" color={color} size={size} />)
          }} />
          <Tab.Screen name="counter" component={Counter} options={{

          }} />
        </Tab.Navigator>
      </NavigationContainer>
    </Provider>
  )
};

export default App;