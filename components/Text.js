import React from "react";
import { StyleSheet, Text } from "react-native";

const MyText = (props) => <Text {...props} style={[...props.style, styles.text]}/>

const styles = StyleSheet.create({
  text: {

  }
})

export default MyText;