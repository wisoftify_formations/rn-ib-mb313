import React from "react";
import { TouchableOpacity, Text, StyleSheet, View } from "react-native";

//mission: ajouter les props color et onPress
const MyButton = ({title, color, onPress, style}) => {
  return (
    <TouchableOpacity onPress={onPress ?? (() => {})}
                      style={[styles.container, style ?? {}, {backgroundColor: color ?? "#0000AA"}]}>
      <Text style={styles.text}>{title}</Text>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 5,
    marginHorizontal: 5
  },
  text: {
    color: "white",
    fontSize: 22
  }
});

export default MyButton;