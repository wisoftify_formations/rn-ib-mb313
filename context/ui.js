import {createContext, useEffect, useContext, useReducer} from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";//npm i @react-native-async-storage/async-storage

const Context = createContext();

const Default = {darkmode: true};

function reducer(state, action) {
  switch (action.type) {
    case "SET_DARKMODE_ON": return ({...state, darkmode: true});
    case "SET_DARKMODE_OFF": return ({...state, darkmode: false});
    case "SET_DARKMODE": return ({...state, darkmode: action.value});
    default: return state;
  }
}

const Provider = ({children}) => {
  const [ctx, setCtx] = useReducer(reducer, {...Default});

  //récupération valeur darkmode dans le asyncStorage
  useEffect(() => {
    (async () => {
      try {
        const darkmode = await AsyncStorage.getItem("darkmode");
        if (darkmode === "false") setCtx({type: "SET_DARKMODE_OFF"});
        else setCtx({type: "SET_DARKMODE_ON"});
      } catch (e) {
        setCtx({type: "SET_DARKMODE_ON"});
      }
    })()
  }, [])

  //sauvegarde valeur darkmode dans le asyncStorage
  useEffect(() => {
    AsyncStorage.setItem("darkmode", ctx.darkmode ? "true" : "false");
  }, [ctx.darkmode])

  return (
    <Context.Provider value={[ctx, setCtx]}>
      {children}
    </Context.Provider>
  )
}

const useUI = () => useContext(Context);

export default useUI;
export {Provider};