import React, {useState, useEffect} from 'react';
import {Text, View, StyleSheet, Button, Platform, NativeModules, DeviceEventEmitter, Share} from "react-native"
import useContext from "../context/ui";

import * as SM from "react-native-size-matters";
//import * as RN from "react-native"

const BatteryManager = NativeModules.BatteryManager;

const Counter = ({route}) => {
  const [ctx, reducer] = useContext();
  const [batteryLevel, setBatteryLevel] = useState(0);

  useEffect(() => {
    BatteryManager.updateBatteryLevel((info) => {
      setBatteryLevel(info.level);
    });
    const e = DeviceEventEmitter.addListener('BatteryStatus', ({ level }) => {
      setBatteryLevel(level);
    });
    return () => e.remove();
  }, []);

  return (
    <View style={[styles(ctx).container, ctx.darkmode ? dark.container : {}]}>
      <Text style={styles(ctx).text}>{ctx.darkmode ? "dark" : "light"}</Text>
      <Text style={styles(ctx).text2(ctx)}>Counter: {route?.params?.id}</Text>
      <Button title="on"
              onPress={() => reducer({type: "SET_DARKMODE", value: true})} />
      <Button title="off"
              onPress={() => reducer({type: "SET_DARKMODE", value: false})} />
      <Text style={styles(ctx).text}>{Platform.OS} v{Platform.Version}</Text>
      <Text style={styles(ctx).text}>Battery: {batteryLevel}%</Text>
      <Button title="Share my battery level" onPress={() => Share.share({message: `Regarde, j'ai ${batteryLevel}% de batterie sur mon ${Platform.OS} ${Platform.constants.Manufacturer} ${Platform.constants.Model}`})} />
    </View>
  );
}

const styles = (props) => StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "lightblue",
  },
  text: {
    color: props.darkmode ? "white" : "black",
    fontSize: SM.verticalScale(20)
  },
  text2: (props) => ({
    color: props.darkmode ? "white" : "black"
  })
})

const dark = StyleSheet.create({
  container: {
    backgroundColor: "grey"
  }
})
export default Counter;