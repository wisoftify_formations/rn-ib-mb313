import React from "react";
import {createStackNavigator} from "@react-navigation/stack";
import Actus from "./Actus";
import Actu from "./Actu";

const Stack = createStackNavigator();

const ActuNavigator = () => {
  return (
    <Stack.Navigator initialRouteName='actus'>
      <Stack.Screen name='actus' component={Actus} />
      <Stack.Screen name='actu' component={Actu} />
    </Stack.Navigator>
  )
}

export default ActuNavigator;