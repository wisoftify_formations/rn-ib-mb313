import React, {useEffect} from "react";
import {Text, ActivityIndicator, Image, StyleSheet, View} from "react-native";
import useGet from "../../hooks/useGet";
import { useNavigation } from "@react-navigation/native";

const Actu = ({route}) => {
  const navigation = useNavigation();
  const {id} = route.params;
  const [article, loading] = useGet(`http://10.0.2.2:1337/api/actus/${id}?populate=*`);

  useEffect(() => {
    if (!article) return
    navigation.setOptions({title: article.data.attributes.title})
  }, [article])

  console.log(`http://10.0.2.2:1337${article?.data?.attributes?.illustration?.data?.attributes?.url}`);
  if (loading) return <ActivityIndicator />
  return (
    <View style={styles.container}>
      <Image source={{uri: `http://10.0.2.2:1337${article?.data?.attributes?.illustration?.data?.attributes?.url}`}} style={styles.image} />
      <Text style={styles.title}>{article?.data?.attributes?.title}</Text>
      <Text style={styles.content}>{article?.data?.attributes?.content}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {

  },
  image: {
    height: 200,
    width: "100%"
  },
  title: {
    fontSize: 24
  },
  content: {

  }
})
export default Actu;