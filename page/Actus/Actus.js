import React, {useState, useEffect} from 'react';
import {Text, Button, View, ActivityIndicator, Image, StyleSheet, ScrollView} from "react-native"
import { useNavigation } from '@react-navigation/native';
import useGet from "../../hooks/useGet";
import useContext from '../../context/ui';

const Actus = ({route}) => {
  const [ctx] = useContext();
  const navigation = useNavigation();
  const [articles, loading] = useGet("http://10.0.2.2:1337/api/actus?populate=*");

  useEffect(() => {console.log(articles)}, [articles])

  useEffect(() => {

  }, [route]);

  if (loading) return <ActivityIndicator />
  return (
    <ScrollView style={styles.container(ctx)}>
      {articles.data.map(item => (
        <View key={item.id} style={styles.article}>
          <Image source={{uri: `http://10.0.2.2:1337${item.attributes.illustration.data.attributes.url}`}} style={styles.image} />
          <View style={styles.actions}>
            <Text style={styles.title(ctx)}>{item.attributes.title}</Text>
            <Button title="Voir plus" onPress={() => navigation.navigate("actu", {id: item.id})} />
          </View>
        </View>
      ))}
    </ScrollView>
  );
}
const styles = StyleSheet.create({
  container: (ctx) => ({
    backgroundColor: ctx.darkmode ? "grey" : "lightblue",
  }),
  article: {
    marginVertical: 10,
  },  
  actions: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 10,
    marginTop: 5,
  },
  image: {
    height: 200,
    width: "100%"
  },
  title: ctx => ({
    fontSize: 24,
    color: ctx.darkmode ? "white" : "black"
  })
})

export default Actus;