//retourne une promesse qui se résoudra après ms millisecondes
const sleep = (ms) =>  new Promise(resolve => setTimeout(resolve, ms));

export default sleep;