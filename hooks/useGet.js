import React, {useState, useEffect} from "react";
import axios from "axios";

const useGet = (url) => {
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(true);

  const refresh = async () => {
    setLoading(true);
    let res = await axios.get(url);
    setData(res.data);
    console.log(res.data);
    setLoading(false);
  }

  useEffect(() => {
    refresh();
  }, [url])

  return [data, loading, refresh];
}

export default useGet;