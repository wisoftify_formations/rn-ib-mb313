import React, {useState, useEffect} from "react";

const useCount = (defaultValue = 0) => {
  const [count, setCount] = useState(defaultValue);

  //useEffect ...

  const increment = () => setCount(count + 1);
  const decrement = () => setCount(count - 1);

  return {count, increment, decrement};
}

export default useCount;