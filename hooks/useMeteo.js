import React, {useState, useEffect} from "react";
import axios from "axios";

const useMeteo = () => {
  const [data, setData] = useState(null);

  useEffect(() => {
    (async () => {
      let res = await axios.get("https://api.open-meteo.com/v1/forecast?latitude=44.837788&longitude=-0.579180&daily=temperature_2m_max,temperature_2m_min&timezone=GMT");
      setData(res.data);
      console.log(res.data);
    })()
  }, [])

  return data;
}

export default useMeteo;